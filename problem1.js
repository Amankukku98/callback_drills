/*
Q1. Create a new file data.json
    { 
        "max": {
            colors: ['Orange', 'Red']
        }
    }
*/


// first method  using fs.writeFile() method.


var fs = require('fs');
var path = require('path');
var data = {
    max: {
        colors: ['orange', 'Red']
    }
}
fs.writeFile(path.join(__dirname, "./data.json"), JSON.stringify(data), (err) => {
    if (err) {
        console.log('error', err);
    }
    else {
        console.log("File created successfully!");
    }
});


// Q2. Copy the contents of the data.json into a new folder output/data.json and delete the original file.

fs.readFile(path.join(__dirname, "./data.json"), 'utf8', (err, data) => {
    if (err) {
        console.log(err);
    }
    else {
        let newData = JSON.parse(data);

        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("Data is added successfully into the output directory");
            }
        });
    }
});

setTimeout(() => {
    fs.unlink('data.json', function (err) {
        if (err) throw err;
        console.log('File deleted successfully!');
    });
}, 1000);


// Q3. Write a function that takes a person name and fav color as parameters and writes to data.json file
//     (Do not replace the old content).

function addColor(data, pname, fcolors) {
    if (data[pname]) {
        data[pname].colors = fcolors;
    }
    else {
        data[pname] = { colors: fcolors };
    }
    return data;
}

fs.readFile(path.join(__dirname, "./output/data.json"), (err, data) => {
    if (err) {
        console.log(err);
    }
    else {

        const newData = addColor(JSON.parse(data), "aman kumar", ["White", "Black"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), (err) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log("Person name and favorate color is added successfully into data.json");
            }
        })
    }
})

/*
 Q4. Write a function that takes a person name and fav hobby as param and add that hobby as a separate key and write to data.json.
    (Do not replace the old content).*/

function addHobby(data, pname, fhobbies) {
    if (data[pname]) {
        data[pname].hobbies = fhobbies;
    }
    else {
        data[pname] = { hobbies: fhobbies };
    }
    return data;
}

fs.readFile(path.join(__dirname, "./output/data.json"), (err, data) => {
    if (err) {
        console.log(err);
    }
    else {

        const newData = addHobby(JSON.parse(data), "aman kumar", ["Travelling", "Singing"]);
        fs.writeFile(path.join(__dirname, "./output/data.json"), JSON.stringify(newData), (err) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log("Person name and Hobby is added successfully into data.json");
            }
        })
    }
})
